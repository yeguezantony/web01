<?php

  session_start();
 
  require '../conexion.php';
// Si el usuario ya ha iniciado sesión, redirigirlo a la página principal
if (isset($_SESSION['user_id'])) {
    header("Location: ../home/pag_principal.php");
    exit();
}
  if (!empty($_POST['username']) && !empty($_POST['password'])) {
    $records = $conn->prepare('SELECT id, username, password FROM users WHERE username = :username');
    $records->bindParam(':username', $_POST['username']);
    $records->execute();
    $results = $records->fetch(PDO::FETCH_ASSOC);

    $message = '';

    if (count($results) > 0 && password_verify($_POST['password'], $results['password'])) {
      $_SESSION['user_id'] = $results['id'];
      header("Location: ../home/pag_principal.php");
    } else {
      $message = 'Sorry, those credentials do not match';
    }
  }

?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Iniciar Sesión - Half-Life 2</title>
    <style>
        body {
            background-color: #000;
            color: #FFA500;
            font-family: Arial, sans-serif;
            text-align: center;
            padding: 20px;
        }
        
        .container {
            background-color: #000;
            border: 2px solid #FFA500;
            padding: 20px;
            width: 300px;
            margin: 0 auto;
            box-shadow: 0px 0px 10px 2px #FFA500;
        }
        
        h1 {
            font-size: 24px;
        }
        
        input[type="text"],
        input[type="password"] {
            width: 100%;
            padding: 10px;
            margin: 10px 0;
            background-color: #333;
            border: none;
            color: #FFA500;
        }
        
        input[type="submit"] {
            background-color: #FFA500;
            color: #000;
            border: none;
            padding: 10px 20px;
            cursor: pointer;
        }
        
        .previous-button {
            position: absolute;
            top: 20px;
            left: 20px;
            color: #FFA500;
            text-decoration: none;
        }
    </style>
</head>
<body>
    <a class="previous-button" href="../index.php">Anterior</a>
    <div class="container">
        <h1>Iniciar Sesión</h1>
        <form action="login.php" method="post">
            <input name="username" type="text" placeholder="Nombre de usuario" required>
            <input name="password" type="password" placeholder="Contraseña" required>
            <input type="submit" value="Iniciar Sesión">
        </form>
    </div>
</body>
</html>

