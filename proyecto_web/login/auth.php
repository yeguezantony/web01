<?php
// Verifica si el usuario está autenticado
function isLoggedIn() {
  return isset($_SESSION['user_id']); 
}

// Redirige a la página de login si no está autenticado 
function checkLoggedIn() {
  if (!isLoggedIn()) {
    $root_path = "/proyecto_web"; 
    header("Location: {$root_path}/index.php");
    exit();
  }
}
?>