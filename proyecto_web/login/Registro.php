<?php

  require '../conexion.php';

  $message = '';

  if (!empty($_POST['username']) && !empty($_POST['password'])) {
    $sql = "INSERT INTO users (username, password) VALUES (:username, :password)";
    $stmt = $conn->prepare($sql);
    $stmt->bindParam(':username', $_POST['username']);
    $password = password_hash($_POST['password'], PASSWORD_BCRYPT);
    $stmt->bindParam(':password', $password);

    if ($stmt->execute()) {
      $message = 'Successfully created new user';
    } else {
      $message = 'Sorry there must have been an issue creating your account';
    }
  }
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Registrarse - Half-Life 2</title>
    <style>
        body {
            background-color: #000;
            color: #FFA500;
            font-family: Arial, sans-serif;
            text-align: center;
            padding: 20px;
        }
        
        .container {
            background-color: #000;
            border: 2px solid #FFA500;
            padding: 20px;
            width: 300px;
            margin: 0 auto;
            box-shadow: 0px 0px 10px 2px #FFA500;
        }
        
        h1 {
            font-size: 24px;
        }
        
        input[type="text"],
        input[type="password"] {
            width: 100%;
            padding: 10px;
            margin: 10px 0;
            background-color: #333;
            border: none;
            color: #FFA500;
        }
        
        input[type="submit"] {
            background-color: #FFA500;
            color: #000;
            border: none;
            padding: 10px 20px;
            cursor: pointer;
        }
        
        .previous-button {
            position: absolute;
            top: 20px;
            left: 20px;
            color: #FFA500;
            text-decoration: none;
        }
    </style>
</head>
<body>
    <a class="previous-button" href="../index.php">Anterior</a>
    <div class="container">
        <h1>Registrarse</h1>
        <form action="Registro.php" method="post">
            <input name="username" type="text" placeholder="Nombre de usuario" required>
            <input name="password" type="password" placeholder="Contraseña" required>
            <input name="confirm_password" type="password" placeholder="Confirmar contraseña" required>
            <input type="submit" value="Registrarse">
        </form>
    </div>
</body>
</html>
