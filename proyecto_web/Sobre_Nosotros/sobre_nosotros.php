<?php

// Iniciar la sesión
session_start(); 

// Incluir el archivo de autenticación
require '../login/auth.php';

// Verificar que el usuario haya iniciado sesión 
checkLoggedIn();

?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title>Proyecto Web</title>
    <meta name="description" content="Figma htmlGenerator">
    <meta name="Jose Camargo, Antony Yeguez, Greeberth Linares" content="htmlGenerator">
    <link href="https://fonts.googleapis.com/css?family=Nunito&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="styles.css">
    <style>
        /*
          Figma Background for illustrative/preview purposes only.
          You can remove this style tag with no consequence
        */
        body {background: #E5E5E5; }
    </style>
</head>
<body>
    <div class="e55_196">
        <div class="e55_197">
            <div class="e55_198"></div>
            <div class="e55_199"></div>
            <div class="e60_2"></div>
            <div class="e55_200">
                <span class="e55_201">Somos Estudiantes con el propósito de aprender sobre diseño y creación de páginas Web, que se esfuerzan por avanzar en su carrera, esta página Web es una muestra de ese esfuerzo, a pesar de ser un prototipo muy básico y con cosas por mejorar ofrecemos lo mejor que podemos dar en el tiempo acordado para crear un modelo funcional y sólido.</span>
                <span class="e60_3">Diseño y código de la Página: José Camargo
Sistema de seguridad y de registro: Antony Yeguez y Greeberth Linares</span>
            </div>
        </div>
        <div class="e55_214">
            <div class="e55_215"></div>
            <a href="../home/pag_principal.php"><span class="e55_216">Inicio</span></a>
            <div class="e55_217"></div>
        </div>
    </div>
</body>
</html>
