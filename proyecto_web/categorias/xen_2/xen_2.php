<?php

// Iniciar la sesión
session_start(); 

// Incluir el archivo de autenticación
require 'C:/xampp/htdocs/proyecto_web/login/auth.php';

// Verificar que el usuario haya iniciado sesión 
checkLoggedIn();

?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title>Proyecto Web</title>
    <meta name="description" content="Figma htmlGenerator">
    <meta name="Jose Camargo, Antony Yeguez, Greeberth Linares" content="htmlGenerator">
    <link href="https://fonts.googleapis.com/css?family=Nunito&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="styles.css">
    <style>
        /*
          Figma Background for illustrative/preview purposes only.
          You can remove this style tag with no consequence
        */
        body {background: #E5E5E5; }
    </style>
</head>
<body>
    <div class="e44_342">
        <div class="e44_343">
            <div class="e44_344"></div>
        </div>
        <div class="e44_345"></div>
        <span class="e44_346">Elige con un simple click sobre el personaje sobre el que quieres conocer mas información</span>
        <div class="e44_347">
            <a href="entrada_1/entrada_1.php">
                <div class="e44_348"></div>
                <div class="e44_349"></div>
                <div class="e44_350"></div>
                <div class="e44_351"></div>
                <span class="e44_352">Ver contenido</span>
                <span class="e44_353">Barnacle</span>
            </a>
        </div>
        <div class="e44_354">
            <a href="entrada_4/entrada_4.php">
                <div class="e44_355"></div>
                <div class="e44_356"></div>
                <div class="e44_357"></div>
                <div class="e44_358"></div>
                <span class="e44_359">Ver contenido</span>
                <span class="e44_360">Hormiga León Guardiana</span>
            </a>
        </div>
        <div class="e44_375">
            <a href="entrada_2/entrada_2.php">
                <div class="e44_376"></div>
                <div class="e44_377"></div>
                <div class="e44_378"></div>
                <div class="e44_379"></div>
                <span class="e44_380">Ver contenido</span>
                <span class="e44_381">Hormiga León Soldado</span>
            </a>
        </div>
        <div class="e44_382">
            <a href="entrada_3/entrada_3.php">
                <div class="e44_383"></div>
                <div class="e44_384"></div>
                <div class="e44_385"></div>
                <div class="e44_386"></div>
                <span class="e44_387">Ver contenido</span>
                <span class="e44_388">Hormiga León Obrera</span>
            </a>
        </div>
        <div class="e44_389">
            <a href="../categorias.php">
                <div class="e44_390"></div>
                <span class="e44_391">Categorias</span>
            </a>
            <div class="e47_409"></div>
        </div>
        <div class="e44_397">
            <a href="../../home/pag_principal.php">
                <div class="e44_398"></div>
                <span class="e44_399">Inicio</span>
            </a>
            <div class="e44_400"></div>
        </div>
        <div class="e44_393">
            <a href="../xen_1/xen_1.php">
                <span class="e44_394"></span>
                <span class="e47_407">Anterior</span>
            </a>
            <div class="e44_396"></div>
        </div>
    </div>
</body>
</html>

