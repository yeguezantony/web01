<?php

// Iniciar la sesión
session_start(); 

// Incluir el archivo de autenticación
require 'C:/xampp/htdocs/proyecto_web/login/auth.php';

// Verificar que el usuario haya iniciado sesión 
checkLoggedIn();

?>
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <title>Proyecto Web</title>
  <meta name="description" content="Figma htmlGenerator">
  <meta name="Jose Camargo, Antony Yeguez, Greeberth Linares" content="htmlGenerator">
  <link href="https://fonts.googleapis.com/css?family=Nunito&display=swap" rel="stylesheet">
  <link rel="stylesheet" href="styles.css">
  <style>
    /*
      Figma Background for illustrative/preview purposes only.
      You can remove this style tag with no consequence
    */
    body {background: #E5E5E5; }
  </style>
</head>
<body>
  <div class="e52_1169">
    <div class="e52_1170">
      <div class="e52_1171"></div>
    </div>
    <div class="e52_1172">
      <div class="e52_1173"></div>
      <div class="e52_1174">
        <span class="e52_1175">Las Hormigas León son criaturas sociales que viven en grandes colonias subterráneas. Son ciegas y utilizan feromonas y vibraciones para identificar a otros seres vivos. Tienen varios tipos, algunos de los cuales son castas separadas.</span>
      </div>
    </div>
    <div class="e52_1176">
      <div class="e52_1177"></div>
      <div class="e52_1178"></div>
    </div>
    <div class="e55_150">
      <div class="e55_151"></div>
      <!-- Agrega un enlace al otro HTML en el atributo "href" sin espacios adicionales -->
      <a href="../xen_2.php"><span class="e55_140"><span class="e55_152">Anterior</span></a>
      <div class="e55_153"></div>
    </div>
  </div>
</body>
</html>
