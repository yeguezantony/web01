<?php

// Iniciar la sesión
session_start(); 

// Incluir el archivo de autenticación
require 'C:/xampp/htdocs/proyecto_web/login/auth.php';

// Verificar que el usuario haya iniciado sesión 
checkLoggedIn();

?>
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <title>Proyecto Web</title>
  <meta name="description" content="Figma htmlGenerator">
  <meta name="Jose Camargo, Antony Yeguez, Greeberth Linares" content="htmlGenerator">
  <link href="https://fonts.googleapis.com/css?family=Nunito&display=swap" rel="stylesheet">
  <link rel="stylesheet" href="styles.css">
  <style>
    /*
      Figma Background for illustrative/preview purposes only.
      You can remove this style tag with no consequence
    */
    body {background: #E5E5E5; }
  </style>
</head>
<body>
  <div class="e52_1191">
    <div class="e52_1192">
      <div class="e52_1193"></div>
    </div>
    <div class="e52_1194">
      <div class="e52_1195"></div>
      <div class="e52_1196">
        <span class="e52_1197">La Hormiga León Obrera es un tipo de Hormiga León, que sólo se encuentran en o cerca de los nidos de Hormiga León en el que realizan diversas tareas, como atender a las Larvas. Producen un ácido corrosivo (que también es una neurotoxina) que utilizan para crear túneles en el nido y también para protegerse de los atacantes.</span>
      </div>
    </div>
    <div class="e52_1198">
      <div class="e52_1199"></div>
      <div class="e52_1200"></div>
    </div>
    <div class="e55_154">
      <div class="e55_155"></div>
      <!-- Agrega un enlace al otro HTML en el atributo "href" sin espacios adicionales -->
      <a href="../xen_2.php"><span class="e55_156">Anterior</span></a>
      <div class="e55_157"></div>
    </div>
  </div>
</body>
</html>
