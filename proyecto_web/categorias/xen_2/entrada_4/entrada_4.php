<?php

// Iniciar la sesión
session_start(); 

// Incluir el archivo de autenticación
require 'C:/xampp/htdocs/proyecto_web/login/auth.php';

// Verificar que el usuario haya iniciado sesión 
checkLoggedIn();

?>
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <title>Proyecto Web</title>
  <meta name="description" content="Figma htmlGenerator">
  <meta name="Jose Camargo, Antony Yeguez, Greeberth Linares" content="htmlGenerator">
  <link href="https://fonts.googleapis.com/css?family=Nunito&display=swap" rel="stylesheet">
  <link rel="stylesheet" href="styles.css">
  <style>
    /*
      Figma Background for illustrative/preview purposes only.
      You can remove this style tag with no consequence
    */
    body {background: #E5E5E5; }
  </style>
</head>
<body>
  <div class="e52_1213">
    <div class="e52_1214">
      <div class="e52_1215"></div>
    </div>
    <div class="e52_1216">
      <div class="e52_1217"></div>
      <div class="e52_1218">
        <span class="e52_1219">La Hormiga León Guardia (también llamada Myrmidonts por los Vortigaunts) es una variante mucho más grande y más fuerte que la Hormiga León Soldado, y es un enemigo formidable. Dentro de la sociedad, los guardias defienden el perímetro y las entradas exteriores a los nidos, también controlan y mantienen el orden dentro de la colonia. Los Vortigaunts extraen los Ferópodos de los Guardias caídos.</span>
      </div>
    </div>
    <div class="e52_1220">
      <div class="e52_1221"></div>
      <div class="e52_1222"></div>
    </div>
    <div class="e55_158">
      <div class="e55_159"></div>
      <!-- Agrega un enlace al otro HTML en el atributo "href" sin espacios adicionales -->
      <a href="../xen_2.php"><span class="e55_160">Anterior</span></a>
      <div class="e55_161"></div>
    </div>
  </div>
</body>
</html>
