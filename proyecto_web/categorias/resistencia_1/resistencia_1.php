<?php

// Iniciar la sesión
session_start(); 

// Incluir el archivo de autenticación
require 'C:/xampp/htdocs/proyecto_web/login/auth.php';

// Verificar que el usuario haya iniciado sesión 
checkLoggedIn();

?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title>Proyecto Web</title>
    <meta name="description" content="Figma htmlGenerator">
    <meta name="Jose Camargo, Antony Yeguez, Greeberth Linares" content="htmlGenerator">
    <link href="https://fonts.googleapis.com/css?family=Nunito&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="styles.css">
    <style>
        /*
          Figma Background for illustrative/preview purposes only.
          You can remove this style tag with no consequence
        */
        body {background: #E5E5E5; }
    </style>
</head>
<body>
    <div class="e17_124">
        <div class="e17_125">
            <div class="e17_126"></div>
            <div class="e17_127"></div>
            <div class="e17_128">
                <span class="e17_129">Elige con un simple click sobre el personaje sobre el que quieres conocer más información</span>
            </div>
        </div>
        <div class="e17_130">
            <a href="entrada_1/entrada_1.php">
                <div class="e17_131"></div>
                <div class="e17_132"></div>
                <div class="e17_133"></div>
                <span class="e17_134">Ver contenido</span>
            </a>
            <div class="e17_135"></div>
            <span class="e17_136">Gordon Freeman</span>
        </div>
        <div class="e17_137">
            <a href="entrada_4/entrada_4.php">
                <div class="e17_138"></div>
                <div class="e17_139"></div>
                <div class="e17_140"></div>
                <span class="e17_141">Ver contenido</span>
            </a>
            <div class="e17_142"></div>
            <span class="e17_143">Eli Vance</span>
        </div>
        <div class="e17_144">
            <a href="entrada_5/entrada_5.php">
                <div class="e17_145"></div>
                <div class="e17_146"></div>
                <div class="e17_147"></div>
                <span class="e17_148">Ver contenido</span>
            </a>
            <div class="e17_149"></div>
            <span class="e17_150">Barney Calhoun</span>
        </div>
        <div class="e17_151">
            <a href="entrada_6/entrada_6.php">
                <div class="e17_152"></div>
                <div class="e17_153"></div>
                <div class="e17_154"></div>
                <span class="e17_155">Ver contenido</span>
            </a>
            <div class="e17_156"></div>
            <span class="e17_157">Judith Mossman</span>
        </div>
        <div class="e17_158">
            <a href="entrada_2/entrada_2.php">
                <div class="e17_159"></div>
                <div class="e17_160"></div>
                <div class="e17_161"></div>
                <span class="e17_162">Ver contenido</span>
            </a>
            <div class="e17_163"></div>
            <span class="e17_164">Alyx Vance</span>
        </div>
        <div class="e17_165">
            <a href="entrada_3/entrada_3.php">
                <div class="e17_166"></div>
                <div class="e17_167"></div>
                <div class="e17_168"></div>
                <span class="e17_169">Ver contenido</span>
            </a>
            <div class="e17_170"></div>
            <span class="e17_171">Isaac Kleiner</span>
        </div>
        <div class="e17_172">
            <a href="../resistencia_2/resistencia_2.php">
                <div class="e17_173"></div>
                <span class="e17_174">Siguiente</span>
            </a>
            <div class="e17_175"></div>
        </div>
        <div class="e17_176">
            <a href="../categorias.php">
                <div class="e17_177"></div>
                <span class="e17_178">Categorías</span>
            </a>
            <div class="e17_179"></div>
        </div>
        <div class="e17_180">
            <a href="../../home/pag_principal.php">
                <div class="e17_181"></div>
                <span class="e17_182">Inicio</span>
            </a>
            <div class="e17_183"></div>
        </div>
        <span class="e40_82">La Resistencia es una poderosa red de Humanos y Vortigaunts con el objetivo de derrotar al Imperio Combine y quitarle a Wallace Breen el poder.</span>
    </div>
</body>
</html>
