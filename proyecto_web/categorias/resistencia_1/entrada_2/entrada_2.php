<?php

// Iniciar la sesión
session_start(); 

// Incluir el archivo de autenticación
require 'C:/xampp/htdocs/proyecto_web/login/auth.php';

// Verificar que el usuario haya iniciado sesión 
checkLoggedIn();

?>
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <title>Proyecto Web</title>
  <meta name="description" content="Figma htmlGenerator">
  <meta name="Jose Camargo, Antony Yeguez, Greeberth Linares" content="htmlGenerator">
  <link href="https://fonts.googleapis.com/css?family=Nunito&display=swap" rel="stylesheet">
  <link rel="stylesheet" href="styles.css">
  <style>
    /* Figma Background for illustrative/preview purposes only.
       You can remove this style tag with no consequence */
    body {background: #E5E5E5; }
  </style>
</head>
<body>
  <div class="e49_508">
    <div class="e49_509">
      <div class="e49_510"></div>
    </div>
    <div class="e49_511">
      <div class="e49_512"></div>
      <div class="e49_513">
        <span class="e49_514">Alyx Vance es un personaje ficticio del videojuego Half-Life 2 de Valve, y de sus siguientes episodios, Half-Life 2: Episode One, Half-Life 2: Episode Two y Half-Life: Alyx (en este último como la protagonista principal). Es interpretada por Merle Dandridge y por Ozioma Akagha en Half-Life: Alyx. Alyx es retratada como una mujer joven, se supone que de ascendencia étnica variada, exactamente de ascendencia malaya y africana, de 24 años, y es una figura destacada en la resistencia humana contra el gobierno de la raza alienígena, llamada "La Alianza" y su administrador, el Dr. Wallace Breen.</span>
      </div>
    </div>
    <div class="e49_518">
      <div class="e49_519"></div>
      <div class="e49_520"></div>
    </div>
    <!-- Agregamos el enlace alrededor del texto "Anterior" -->
    <a href="../resistencia_1.php" class="e55_6">
      <div class="e55_7"></div>
      <span class="e55_8">Anterior</span>
      <div class="e55_9"></div>
    </a>
  </div>
</body>
</html>
