<?php

// Iniciar la sesión
session_start(); 

// Incluir el archivo de autenticación
require 'C:/xampp/htdocs/proyecto_web/login/auth.php';

// Verificar que el usuario haya iniciado sesión 
checkLoggedIn();

?>
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <title>Proyecto Web</title>
  <meta name="description" content="Figma htmlGenerator">
  <meta name="Jose Camargo, Antony Yeguez, Greeberth Linares" content="htmlGenerator">
  <link href="https://fonts.googleapis.com/css?family=Nunito&display=swap" rel="stylesheet">
  <link rel="stylesheet" href="styles.css">
  <style>
    /* Figma Background for illustrative/preview purposes only.
       You can remove this style tag with no consequence */
    body {background: #E5E5E5; }
  </style>
</head>
<body>
  <div class="e49_562">
    <div class="e49_563">
      <div class="e49_564"></div>
    </div>
    <div class="e49_565">
      <div class="e49_566"></div>
      <div class="e49_567">
        <span class="e49_568">El Dr. Eli Vance es un personaje ficticio de los videojuegos Half-Life y Half-Life 2. Es doblado en la versión inglesa por Robert Guillaume (por James Moses Black en Half-Life: Alyx, debido al fallecimiento del anterior). Vance es un físico e investigador brillante, que aparentemente estudió en Harvard. Parece que es un hombre afrodescendiente, y que está a finales de los cincuenta años o a principio de los sesenta, con un corto pelo gris, barba y bigote. Vance lleva puesto una prótesis, de no muy alta calidad, para sustituir su pierna izquierda bajo la rodilla, que fue perdida cuando fue atacado por un Bullsquid ayudando a su amigo y científico Isaac Kleiner a escapar de los laboratorios Black Mesa, vistos en Half-Life.
Él es el padre de Alyx Vance. Su antigua esposa, Azian, también vivió dentro de las instalaciones de Black Mesa, pero murió después del incidente.
        </span>
      </div>
    </div>
    <div class="e49_569">
      <div class="e49_570"></div>
      <div class="e49_571"></div>
    </div>
    <!-- Agregamos el enlace alrededor del texto "Anterior" -->
    <a href="../resistencia_1.php" class="e55_10">
      <div class="e55_11"></div>
      <span class="e55_12">Anterior</span>
      <div class="e55_13"></div>
    </a>
  </div>
</body>
</html>
