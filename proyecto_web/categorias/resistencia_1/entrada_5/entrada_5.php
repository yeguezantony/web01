<?php

// Iniciar la sesión
session_start(); 

// Incluir el archivo de autenticación
require 'C:/xampp/htdocs/proyecto_web/login/auth.php';

// Verificar que el usuario haya iniciado sesión 
checkLoggedIn();

?>
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <title>Proyecto Web</title>
  <meta name="description" content="Figma htmlGenerator">
  <meta name="Jose Camargo, Antony Yeguez, Greeberth Linares" content="htmlGenerator">
  <link href="https://fonts.googleapis.com/css?family=Nunito&display=swap" rel="stylesheet">
  <link rel="stylesheet" href="styles.css">
  <style>
    /* Figma Background for illustrative/preview purposes only.
       You can remove this style tag with no consequence */
    body {background: #E5E5E5; }
  </style>
</head>
<body>
  <div class="e49_584">
    <div class="e49_585">
      <div class="e49_586"></div>
      <div class="e49_606"></div>
    </div>
    <div class="e49_587">
      <div class="e49_588"></div>
      <div class="e49_589">
        <span class="e49_590">Barney Calhoun es un personaje ficticio en la serie de videojuegos Half-Life. Es doblado en la versión inglesa por Michael Shapiro.
Aunque al principio fue considerado un personaje "desechable" (de hecho, una clase entera de personajes desechables), Barney ha desempeñado papeles cada vez más importantes a medida que la serie ha ido avanzando. De hecho Barney Calhoun es el personaje principal de Half-Life: Blue Shift, una expansión del Half-Life original. Barney, en la primera entrega de Half-Life apenas se le ve al principio del juego, pero a medida que el juego avanza y en su papel en el ya mencionado Blue Shift su importancia es cada vez más notable. Más adelante, en Half-Life 2 llega a desempeñar el puesto de jefe de los rebeldes.
        </span>
      </div>
    </div>
    <div class="e49_591">
      <div class="e49_592"></div>
      <div class="e49_593"></div>
    </div>
    <!-- Agregamos el enlace alrededor del texto "Anterior" -->
    <a href="../resistencia_1.php" class="e55_18">
      <div class="e55_19"></div>
      <span class="e55_20">Anterior</span>
      <div class="e55_21"></div>
    </a>
  </div>
</body>
</html>
