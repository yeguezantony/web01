<?php

// Iniciar la sesión
session_start(); 

// Incluir el archivo de autenticación
require 'C:/xampp/htdocs/proyecto_web/login/auth.php';

// Verificar que el usuario haya iniciado sesión 
checkLoggedIn();

?>
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <title>Proyecto Web</title>
  <meta name="description" content="Figma htmlGenerator">
  <meta name="Jose Camargo, Antony Yeguez, Greeberth Linares" content="htmlGenerator">
  <link href="https://fonts.googleapis.com/css?family=Nunito&display=swap" rel="stylesheet">
  <link rel="stylesheet" href="styles.css">
  <style>
    /* Figma Background for illustrative/preview purposes only.
       You can remove this style tag with no consequence */
    body {background: #E5E5E5; }
  </style>
</head>
<body>
  <div class="e49_533">
    <div class="e49_534">
      <div class="e49_535"></div>
    </div>
    <div class="e49_536">
      <div class="e49_537"></div>
      <div class="e49_538">
        <span class="e49_539">El Doctor Isaac Kleiner es un personaje ficticio de los videojuegos Half-Life y Half-Life 2. Es doblado en la versión inglesa por Harry S. Robins.
Él es uno de los pocos sobrevivientes del incidente que ocurrió en Black Mesa, y de ser el autor de varias funciones sobre la teletransportación y el viaje interdimensional, es una figura clave en la comunidad científica de la Resistencia contra el imperio de La Alianza. El Dr. Eli Vance lo llama cariñosamente en varias ocasiones como “Izzy”. Kleiner logró escapar de Black Mesa junto con el doctor Vance, el doctor Rosenberg y Barney Calhoun en un automóvil con el logo de Black Mesa. Mientras Gordon trabajaba en el MIT, Kleiner fue su mentor y pudo comprobar sus habilidades en física avanzada.</span>
      </div>
    </div>
    <div class="e49_543">
      <div class="e49_544"></div>
      <div class="e49_545"></div>
    </div>
    <!-- Agregamos el enlace alrededor del texto "Anterior" -->
    <a href="../resistencia_1.php" class="e55_14">
      <div class="e55_15"></div>
      <span class="e55_16">Anterior</span>
      <div class="e55_17"></div>
    </a>
  </div>
</body>
</html>
