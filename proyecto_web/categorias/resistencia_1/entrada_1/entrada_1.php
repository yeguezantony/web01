<?php

// Iniciar la sesión
session_start(); 

// Incluir el archivo de autenticación
require 'C:/xampp/htdocs/proyecto_web/login/auth.php';

// Verificar que el usuario haya iniciado sesión 
checkLoggedIn();

?>
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <title>Proyecto Web</title>
  <meta name="description" content="Figma htmlGenerator">
  <meta name="Jose Camargo, Antony Yeguez, Greeberth Linares" content="htmlGenerator">
  <link href="https://fonts.googleapis.com/css?family=Nunito&display=swap" rel="stylesheet">
  <link rel="stylesheet" href="styles.css">
  <style>
    /* Figma Background for illustrative/preview purposes only.
       You can remove this style tag with no consequence */
    body {background: #E5E5E5; }
  </style>
</head>
<body>
  <div class="e48_450">
    <div class="e48_451">
      <div class="e48_452"></div>
    </div>
    <div class="e48_496">
      <div class="e48_453"></div>
      <div class="e48_454">
        <span class="e48_455">Gordon es nativo de Seattle, Washington, Freeman albergó un interés temprano en la física teórica, como la mecánica cuántica y la Teoría de la relatividad. Sus inspiradores fueron Albert Einstein, Stephen Hawking y Richard Feynman. No tiene dependientes y se graduó del MIT, habiendo obtenido un Ph.D. en física teórica. Después de graduarse del MIT, Gordon viaja a Austria y observa una serie de experimentos de teletransportación realizados por el Instituto de Física Experimental en Innsbruck, pero se desilusiona con el lento ritmo de la investigación de teletransportación en la academia y comienza a buscar trabajo fuera del sector educativo. Por coincidencia, el mentor de Freeman en el MIT, el Dr. Isaac Kleiner, se ha hecho cargo de un proyecto de investigación en un centro de investigación integrado de propiedad estatal conocido como Black Mesa Research Facility y le ofrece un trabajo a Freeman. Él acepta, esperando que al menos parte de la inmensa financiación se destinará a aplicaciones civiles de astrofísica y computación cuántica, lo demás es historia.
        </span>
      </div>
    </div>
    <div class="e48_456">
      <div class="e48_458"></div>
      <div class="e48_501"></div>
    </div>
    <!-- Agregamos el enlace alrededor del texto "Anterior" -->
    <a href="../resistencia_1.php" class="e55_2">
      <div class="e55_3"></div>
      <span class="e55_4">Anterior</span>
      <div class="e55_5"></div>
    </a>
  </div>
</body>
</html>
