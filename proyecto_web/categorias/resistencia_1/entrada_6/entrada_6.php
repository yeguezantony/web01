<?php

// Iniciar la sesión
session_start(); 

// Incluir el archivo de autenticación
require 'C:/xampp/htdocs/proyecto_web/login/auth.php';

// Verificar que el usuario haya iniciado sesión 
checkLoggedIn();

?>
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <title>Proyecto Web</title>
  <meta name="description" content="Figma htmlGenerator">
  <meta name="Jose Camargo, Antony Yeguez, Greeberth Linares" content="htmlGenerator">
  <link href="https://fonts.googleapis.com/css?family=Nunito&display=swap" rel="stylesheet">
  <link rel="stylesheet" href="styles.css">
  <style>
    /* Figma Background for illustrative/preview purposes only.
       You can remove this style tag with no consequence */
    body {background: #E5E5E5; }
  </style>
</head>
<body>
  <div class="e49_607">
    <div class="e49_608">
      <div class="e49_609"></div>
      <div class="e49_610"></div>
    </div>
    <div class="e49_611">
      <div class="e49_612"></div>
      <div class="e49_613">
        <span class="e49_614">La Dra. Judith Mossman (con la voz de Michelle Forbes ) se presenta en Half-Life 2 como una física que trabaja con Eli Vance en el Centro de Investigación Black Mesa East. Aunque aparentemente es amigable con otros científicos, su actitud condescendiente hacia los legos molesta a Alyx. En el transcurso del juego, ella se revela como una triple agente que traiciona a la resistencia en un intento de formar una alianza con el Dr. Breen y luego, a su vez, lo traiciona. En los episodios siguientes, vuelve a trabajar para la resistencia en un lugar remoto.
        </span>
      </div>
    </div>
    <div class="e49_615">
      <div class="e49_616"></div>
      <div class="e49_617"></div>
    </div>
    <!-- Agregamos el enlace alrededor del texto "Anterior" -->
    <a href="../resistencia_1.php" class="e55_22">
      <div class="e55_23"></div>
      <span class="e55_24">Anterior</span>
      <div class="e55_25"></div>
    </a>
  </div>
</body>
</html>
