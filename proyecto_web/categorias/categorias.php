<?php

// Iniciar la sesión
session_start(); 

// Incluir el archivo de autenticación
require '../login/auth.php';

// Verificar que el usuario haya iniciado sesión 
checkLoggedIn();

?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title>Proyecto Web</title>
    <meta name="description" content="Figma htmlGenerator">
    <meta name="Jose Camargo, Antony Yeguez, Greeberth Linares" content="htmlGenerator">
    <link href="https://fonts.googleapis.com/css?family=Nunito&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="styles.css">
    <style>
        /*
          Figma Background for illustrative/preview purposes only.
          You can remove this style tag with no consequence
        */
        body {background: #E5E5E5; }
    </style>
</head>
<body>
    <div class="e19_184">
        <div class="e19_185">
            <div class="e19_186"></div>
            <div class="e19_187"></div>
            <div class="e19_188">
                <span class="e19_189">Elige Entre las Categorias para ver la información de sus Respectivos Integrantes</span>
            </div>
        </div>
        <div class="e19_248">
            <div class="e19_249"></div>
            <a href="resistencia_1/resistencia_1.php"><span class="e19_250">Resistencia: 10</span></a>
            <div class="e24_7"></div>
        </div>
        <div class="e19_253">
            <div class="e19_254"></div>
            <a href="combine_1/combine_1.php"><span class="e19_255">Combine: 13</span></a>
            <div class="e25_2"></div>
        </div>
        <div class="e19_257">
            <div class="e19_258"></div>
            <a href="xen_1/xen_1.php"><span class="e19_259">Xen: 10</span></a>
            <div class="e25_4"></div>
        </div>
        <div class="e19_261">
            <div class="e19_262"></div>
            <a href="gman_1/gman_1.php"><span class="e19_263">Desconocido: 1</span></a>
            <div class="e25_5"></div>
        </div>
        <div class="e19_240">
            <div class="e19_241"></div>
            <a href="../home/pag_principal.php"><span class="e19_242">Inicio</span></a>
            <div class="e19_243"></div>
        </div>
        <div class="e20_265"></div>
        <div class="e20_266"></div>
        <a href="../sobre_nosotros/sobre_nosotros.php"><span class="e20_267">Sobre nosotros</span></a>
        <a href="../contacto/contacto.php"><span class="e20_268">Contacto</span></a>
    </div>
</body>
</html>
