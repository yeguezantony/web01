<?php

// Iniciar la sesión
session_start(); 

// Incluir el archivo de autenticación
require 'C:/xampp/htdocs/proyecto_web/login/auth.php';

// Verificar que el usuario haya iniciado sesión 
checkLoggedIn();

?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title>Proyecto Web</title>
    <meta name="description" content="Figma htmlGenerator">
    <meta name="Jose Camargo, Antony Yeguez, Greeberth Linares" content="htmlGenerator">
    <link href="https://fonts.googleapis.com/css?family=Nunito&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="styles.css">
    <style>
        /*
          Figma Background for illustrative/preview purposes only.
          You can remove this style tag with no consequence
        */
        body {background: #E5E5E5; }
    </style>
</head>
<body>
    <div class="e47_410">
        <div class="e47_411">
            <div class="e47_412"></div>
        </div>
        <div class="e47_413">
            <a href="entrada_1/entrada_1.php">
                <div class="e47_414"></div>
                <div class="e47_415"></div>
                <div class="e47_416"></div>
                <span class="e47_417">Ver contenido</span>
                <span class="e47_419">GMAN</span>
            </a>
        </div>
        <div class="e47_424">
            <a href="../categorias.php">
                <div class="e47_425"></div>
                <span class="e47_426">Categorias</span>
            </a>
            <div class="e47_427"></div>
        </div>
        <div class="e47_428">
            <a href="../../home/pag_principal.php">
                <div class="e47_429"></div>
                <span class="e47_430">Inicio</span>
            </a>
            <div class="e47_431"></div>
        </div>
        <div class="e47_433">
            <span class="e47_434">Elige con un simple click sobre el personaje sobre el que quieres conocer mas información</span>
        </div>
        <div class="e47_432"></div>
    </div>
</body>
</html>
