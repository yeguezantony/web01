<?php

// Iniciar la sesión
session_start(); 

// Incluir el archivo de autenticación
require 'C:/xampp/htdocs/proyecto_web/login/auth.php';

// Verificar que el usuario haya iniciado sesión 
checkLoggedIn();

?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title>Proyecto Web</title>
    <meta name="description" content="Figma htmlGenerator">
    <meta name="Jose Camargo, Antony Yeguez, Greeberth Linares" content="htmlGenerator">
    <link href="https://fonts.googleapis.com/css?family=Nunito&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="styles.css">
    <style>
        /*
          Figma Background for illustrative/preview purposes only.
          You can remove this style tag with no consequence
        */
        body {background: #E5E5E5; }
    </style>
</head>
<body>
    <div class="e52_1235">
        <div class="e52_1236">
            <div class="e52_1237"></div>
        </div>
        <div class="e52_1238">
            <div class="e52_1239"></div>
            <div class="e52_1240">
                <span class="e52_1241">La identidad y los motivos del G-Man siguen sin explicación. Su lealtad también se desconoce hasta el día de hoy, aunque él y sus empleadores son enemigos aparentes del Combine y son vistos como una amenaza por ellos. Al desempeñar el papel de supervisor y eventual empleador, a menudo aparece en lugares extraños e inalcanzables, observando a Gordon Freeman y otros personajes, ayudándolos o obstaculizando periódicamente.</span>
            </div>
        </div>
        <div class="e52_1242">
            <div class="e52_1243"></div>
            <div class="e52_1244"></div>
        </div>
        <div class="e55_162">
            <div class="e55_163"></div>
            <!-- Cambia el atributo href para redirigir a la página deseada -->
            <a href="../gman_1.php"><span class="e55_164">Anterior</span></a>
            <div class="e55_165"></div>
        </div>
    </div>
</body>
</html>
