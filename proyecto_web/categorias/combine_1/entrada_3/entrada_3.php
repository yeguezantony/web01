<?php

// Iniciar la sesión
session_start(); 

// Incluir el archivo de autenticación
require 'C:/xampp/htdocs/proyecto_web/login/auth.php';

// Verificar que el usuario haya iniciado sesión 
checkLoggedIn();

?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title>Proyecto Web</title>
    <meta name="description" content="Figma htmlGenerator">
    <meta name="Jose Camargo, Antony Yeguez, Greeberth Linares" content="htmlGenerator">
    <link href="https://fonts.googleapis.com/css?family=Nunito&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="styles.css">
    <style>
        /*
          Figma Background for illustrative/preview purposes only.
          You can remove this style tag with no consequence
        */
        body {background: #E5E5E5; }
    </style>
</head>
<body>
    <div class="e52_768">
        <div class="e52_769">
            <div class="e52_770"></div>
        </div>
        <div class="e52_771">
            <div class="e52_772"></div>
            <div class="e52_773">
                <span class="e52_774">Los soldados de Overwatch , conocidos como soldados combinados por los ciudadanos y equipos de estabilización por Overwatch Voice , son las unidades básicas de infantería transhumana de Combine Overwatch y el ejército principal de Combine en la Tierra. A diferencia de Protección Civil , cuyas funciones son imponer el orden dentro de los centros de población reprimidos, los soldados de Overwatch tienen la tarea de realizar acciones más peligrosas que requieren habilidad y táctica: desde patrullar las traicioneras fronteras del territorio controlado por Combine hasta asaltar fortalezas controladas por la Resistencia y neutralizar cualquier forma. de amenaza para el Combine.
                Al ser personal militar de Combine, los soldados de Overwatch tienen acceso a equipo de grado militar y son entrenados (o, más exactamente, programados) con entrenamiento de combate táctico avanzado, lo que los convierte en oponentes más formidables y fuertes.
                </span>
            </div>
        </div>
        <div class="e52_775">
            <div class="e52_776"></div>
            <div class="e52_777"></div>
        </div>
        <div class="e55_54">
            <div class="e55_55"></div>
            <!-- Cambia el atributo href para redirigir a la página deseada -->
            <a href="../combine_1.php"><span class="e55_56">Anterior</span></a>
            <div class="e55_57"></div>
        </div>
    </div>
</body>
</html>
