<?php

// Iniciar la sesión
session_start(); 

// Incluir el archivo de autenticación
require 'C:/xampp/htdocs/proyecto_web/login/auth.php';

// Verificar que el usuario haya iniciado sesión 
checkLoggedIn();

?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title>Proyecto Web</title>
    <meta name="description" content="Figma htmlGenerator">
    <meta name="Jose Camargo, Antony Yeguez, Greeberth Linares" content="htmlGenerator">
    <link href="https://fonts.googleapis.com/css?family=Nunito&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="styles.css">
    <style>
        /*
          Figma Background for illustrative/preview purposes only.
          You can remove this style tag with no consequence
        */
        body {background: #E5E5E5; }
    </style>
</head>
<body>
    <div class="e52_814">
        <div class="e52_815">
            <div class="e52_816"></div>
        </div>
        <div class="e52_817">
            <div class="e52_818"></div>
            <div class="e52_819">
                <span class="e52_820">Los Robosierras son pequeños dispositivos de ataque que utilizan afiladas hélices que giran a muy altas velocidades, tanto para propulsarse como para atacar. Sus cuchillas giratorias producen un zumbido agudo, que es a menudo un útil indicador de que se aproximan. Sus hojas son lo suficientemente potentes y filosas como para cortar a través de obstáculos de madera, pero no lo suficiente como para cortar a través de materiales más fuertes como el metal o el hormigón. Están programados con un muy bajo concepto de auto-preservación, golpeándose contra paredes y objetos durante la búsqueda de su objetivo.
                </span>
            </div>
        </div>
        <div class="e52_821">
            <div class="e52_822"></div>
            <div class="e52_823"></div>
        </div>
        <div class="e55_58">
            <div class="e55_59"></div>
            <!-- Cambia el atributo href para redirigir a la página deseada -->
            <a href="../combine_1.php"><span class="e55_60">Anterior</span></a>
            <div class="e55_61"></div>
        </div>
    </div>
</body>
</html>
