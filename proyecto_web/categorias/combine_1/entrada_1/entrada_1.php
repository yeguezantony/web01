<?php

// Iniciar la sesión
session_start(); 

// Incluir el archivo de autenticación
require 'C:/xampp/htdocs/proyecto_web/login/auth.php';

// Verificar que el usuario haya iniciado sesión 
checkLoggedIn();

?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title>Proyecto Web</title>
    <meta name="description" content="Figma htmlGenerator">
    <meta name="Jose Camargo, Antony Yeguez, Greeberth Linares" content="htmlGenerator">
    <link href="https://fonts.googleapis.com/css?family=Nunito&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="styles.css">
    <style>
        /*
          Figma Background for illustrative/preview purposes only.
          You can remove this style tag with no consequence
        */
        body {background: #E5E5E5; }
    </style>
</head>
<body>
    <div class="e49_722">
        <div class="e49_723">
            <div class="e49_724"></div>
        </div>
        <div class="e49_726">
            <div class="e49_727"></div>
            <div class="e49_728">
                <span class="e49_729">El Dr. Wallace Breen (con la voz de Robert Culp ) era el administrador del Centro de Investigación de Black Mesa en el momento del "Incidente de Black Mesa", los eventos descritos en Half-Life , pero no fue visto ni mencionado por su nombre (era en su lugar siempre se le denominará "el Administrador"). Después de la Guerra de las Siete Horas , "negoció" un acuerdo de paz con los Combine que salvó a la humanidad a costa de la esclavitud. El Dr. Breen fue designado gobernante de la Tierra, un títere de los Combine, que tienen poca presencia física en el planeta. En sus mensajes de propaganda a la gente de la Ciudad 17 (apodados "Breencasts"), a menudo se refiere al Combine como "nuestros Benefactores".
                </span>
            </div>
        </div>
        <div class="e49_730">
            <div class="e49_731"></div>
            <div class="e49_732"></div>
        </div>
        <div class="e55_42">
            <div class="e55_43"></div>
            <!-- Cambia el atributo href para redirigir a la página deseada -->
            <a href="../combine_1.php"><span class="e55_44">Anterior</span></a>
            <div class="e55_45"></div>
        </div>
    </div>
</body>
</html>
