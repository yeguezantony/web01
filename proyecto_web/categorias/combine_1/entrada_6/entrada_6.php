<?php

// Iniciar la sesión
session_start(); 

// Incluir el archivo de autenticación
require 'C:/xampp/htdocs/proyecto_web/login/auth.php';

// Verificar que el usuario haya iniciado sesión 
checkLoggedIn();

?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title>Proyecto Web</title>
    <meta name="description" content="Figma htmlGenerator">
    <meta name="Jose Camargo, Antony Yeguez, Greeberth Linares" content="htmlGenerator">
    <link href="https://fonts.googleapis.com/css?family=Nunito&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="styles.css">
    <style>
        /*
          Figma Background for illustrative/preview purposes only.
          You can remove this style tag with no consequence
        */
        body {background: #E5E5E5; }
    </style>
</head>
<body>
    <div class="e52_836">
        <div class="e52_837">
            <div class="e52_838"></div>
        </div>
        <div class="e52_839">
            <div class="e52_840"></div>
            <div class="e52_841">
                <span class="e52_842">Apenas reconocibles como humanos, los Acechadores han sufrido intervenciones quirúrgicas drásticas e implantaciones sintéticas. Las extremidades de un Acechador (manos y piernas inferiores) se han eliminado y sustituido por piezas metálicas, que ofrecen agilidad reducida y resultan en un caminar lento, similar al de un Zombie. Al parecer, son incapaces de hablar, y pronuncian sólo rugidos y gruñidos, probablemente como resultado de la alteración de la mente y la laringe.
                </span>
            </div>
        </div>
        <div class="e52_843">
            <div class="e52_844"></div>
            <div class="e52_845"></div>
        </div>
        <div class="e55_62">
            <div class="e55_63"></div>
            <!-- Cambia el atributo href para redirigir a la página deseada -->
            <a href="../combine_1.php"><span class="e55_64">Anterior</span></a>
            <div class="e55_65"></div>
        </div>
    </div>
</body>
</html>
