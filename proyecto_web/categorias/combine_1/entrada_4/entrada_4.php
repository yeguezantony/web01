<?php

// Iniciar la sesión
session_start(); 

// Incluir el archivo de autenticación
require 'C:/xampp/htdocs/proyecto_web/login/auth.php';

// Verificar que el usuario haya iniciado sesión 
checkLoggedIn();

?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title>Proyecto Web</title>
    <meta name="description" content="Figma htmlGenerator">
    <meta name="Jose Camargo, Antony Yeguez, Greeberth Linares" content="htmlGenerator">
    <link href="https://fonts.googleapis.com/css?family=Nunito&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="styles.css">
    <style>
        /*
          Figma Background for illustrative/preview purposes only.
          You can remove this style tag with no consequence
        */
        body {background: #E5E5E5; }
    </style>
</head>
<body>
    <div class="e52_791">
        <div class="e52_792">
            <div class="e52_793"></div>
        </div>
        <div class="e52_794">
            <div class="e52_795"></div>
            <div class="e52_796">
                <span class="e52_797">EL Overwatch Élite está equipado con un blindaje corporal mejorado, es evidente por el mayor protector de cuello y el aumento de la protección que el traje ofrece. Sus cascos también son drásticamente diferentes a los de los soldados regulares, lo que posiblemente mejora en algunos sentidos, indicado por las dos cúpulas en miniatura situadas en los oídos de los soldados.
                Los élites son más resistentes, también logran una mayor precisión al disparar con sus armas, e infligen un daño más general que los soldados regulares. Por lo general llevan el Rifle de Pulso, y son capaces de utilizar el fuego secundario o sea la Esfera de Energía del arma. Utilizan esta ventaja sin dudarlo y con una precisión mortal.
                </span>
            </div>
        </div>
        <div class="e52_799">
            <div class="e52_800"></div>
            <div class="e52_801"></div>
        </div>
        <div class="e55_50">
            <div class="e55_51"></div>
            <!-- Cambia el atributo href para redirigir a la página deseada -->
            <a href="../combine_1.php"><span class="e55_52">Anterior</span></a>
            <div class="e55_53"></div>
        </div>
    </div>
</body>
</html>
