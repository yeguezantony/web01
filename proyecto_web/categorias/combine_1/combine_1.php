<?php

// Iniciar la sesión
session_start(); 

// Incluir el archivo de autenticación
require 'C:/xampp/htdocs/proyecto_web/login/auth.php';

// Verificar que el usuario haya iniciado sesión 
checkLoggedIn();

?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title>Proyecto Web</title>
    <meta name="description" content="Figma htmlGenerator">
    <meta name="Jose Camargo, Antony Yeguez, Greeberth Linares" content="htmlGenerator">
    <link href="https://fonts.googleapis.com/css?family=Nunito&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="styles.css">
    <style>
        /*
          Figma Background for illustrative/preview purposes only.
          You can remove this style tag with no consequence
        */
        body {background: #E5E5E5; }
    </style>
</head>
<body>
    <div class="e40_22">
        <div class="e40_23">
            <div class="e40_24"></div>
            <div class="e40_25"></div>
            <div class="e40_26">
                <span class="e40_27">Elige con un simple click sobre el personaje sobre el que quieres conocer más información</span>
            </div>
        </div>
        <div class="e40_28">
            <a href="entrada_1/entrada_1.php">
                <div class="e40_29"></div>
                <div class="e40_30"></div>
                <div class="e40_31"></div>
                <span class="e40_32">Ver contenido</span>
            </a>
            <div class="e40_33"></div>
            <span class="e40_34">Wallace Breen</span>
        </div>
        <div class="e40_35">
            <a href="entrada_4/entrada_4.php">
                <div class="e40_94"></div>
                <div class="e40_95"></div>
                <div class="e40_96"></div>
                <div class="e40_38"></div>
                <span class="e40_39">Ver contenido</span>
            </a>
            <span class="e40_41">Combine Elite</span>
        </div>
        <div class="e40_42">
            <a href="entrada_5/entrada_5.php">
                <div class="e40_97"></div>
                <div class="e40_98"></div>
                <div class="e40_99"></div>
                <div class="e40_45"></div>
                <span class="e40_46">Ver contenido</span>
            </a>
            <span class="e40_48">Robosierra</span>
        </div>
        <div class="e40_49">
            <a href="entrada_6/entrada_6.php">
                <div class="e40_100"></div>
                <div class="e40_101"></div>
                <div class="e40_102"></div>
                <div class="e40_52"></div>
                <span class="e40_53">Ver contenido</span>
            </a>
            <span class="e40_55">Stalker</span>
        </div>
        <div class="e40_56">
            <a href="entrada_2/entrada_2.php">
                <div class="e40_85"></div>
                <div class="e40_86"></div>
                <div class="e40_87"></div>
                <div class="e40_59"></div>
                <span class="e40_60">Ver contenido</span>
            </a>
            <span class="e40_62">Metrocop</span>
        </div>
        <div class="e40_63">
            <a href="entrada_3/entrada_3.php">
                <div class="e40_91"></div>
                <div class="e40_92"></div>
                <div class="e40_93"></div>
                <div class="e40_66"></div>
                <span class="e40_67">Ver contenido</span>
            </a>
            <span class="e40_69">Combine soldier</span>
        </div>
        <div class="e40_70">
            <a href="../combine_2/combine_2.php">
                <div class="e40_71"></div>
                <span class="e40_72">Siguiente</span>
            </a>
            <div class="e40_73"></div>
        </div>
        <div class="e40_74">
            <a href="../categorias.php">
                <div class="e40_75"></div>
                <span class="e40_76">Categorías</span>
            </a>
            <div class="e40_77"></div>
        </div>
        <div class="e40_78">
            <a href="../../home/pag_principal.php">
                <div class="e40_79"></div>
                <span class="e40_80">Inicio</span>
            </a>
            <div class="e40_81"></div>
        </div>
        <span class="e40_83">El Imperio Combine, Unión Universal o La Alianza, referida a ella como nuestros benefactores, es el título de un inmenso y poderoso imperio interdimensional, compuesto por una gran variedad de especies esclavas.</span>
    </div>
</body>
</html>
