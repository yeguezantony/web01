<?php

// Iniciar la sesión
session_start(); 

// Incluir el archivo de autenticación
require 'C:/xampp/htdocs/proyecto_web/login/auth.php';

// Verificar que el usuario haya iniciado sesión 
checkLoggedIn();

?>
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <title>Proyecto Web</title>
  <meta name="description" content="Figma htmlGenerator">
  <meta name="Jose Camargo, Antony Yeguez, Greeberth Linares" content="htmlGenerator">
  <link href="https://fonts.googleapis.com/css?family=Nunito&display=swap" rel="stylesheet">
  <link rel="stylesheet" href="styles.css">
  <style>
    /* Figma Background for illustrative/preview purposes only.
       You can remove this style tag with no consequence */
    body {background: #E5E5E5; }
  </style>
</head>
<body>
  <div class="e52_1013">
    <div class="e52_1014">
      <div class="e52_1015"></div>
    </div>
    <div class="e52_1016">
      <div class="e52_1017"></div>
      <div class="e52_1018">
        <span class="e52_1019">Los headcrabs son organismos alienígenas parásitos omnívoros de Xen. Estas criaturas han aparecido en la mayoría de lanzamientos de títulos basados en el universo Half-Life. Gracias a esto, y al hecho de que aparecen con frecuencia a lo largo de la historia, se garantiza que estas pequeñas criaturas quedarán grabadas en la cabeza de los usuarios que juegan uno o más títulos de Half-Life. Esto ha llevado a la comunidad a considerar al Headcrab como una de las criaturas más emblemáticas del universo Half-Life.</span>
      </div>
    </div>
    <div class="e52_1020">
      <div class="e52_1021"></div>
      <div class="e52_1022"></div>
    </div>
    <!-- Agregamos el enlace alrededor del texto "Anterior" -->
    <a href="../xen_1.php" class="e55_122">
      <div class="e55_123"></div>
      <span class="e55_124">Anterior</span>
      <div class="e55_125"></div>
    </a>
  </div>
</body>
</html>
