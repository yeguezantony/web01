<?php

// Iniciar la sesión
session_start(); 

// Incluir el archivo de autenticación
require 'C:/xampp/htdocs/proyecto_web/login/auth.php';

// Verificar que el usuario haya iniciado sesión 
checkLoggedIn();

?>
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <title>Proyecto Web</title>
  <meta name="description" content="Figma htmlGenerator">
  <meta name="Jose Camargo, Antony Yeguez, Greeberth Linares" content="htmlGenerator">
  <link href="https://fonts.googleapis.com/css?family=Nunito&display=swap" rel="stylesheet">
  <link rel="stylesheet" href="styles.css">
  <style>
    /*
      Figma Background for illustrative/preview purposes only.
      You can remove this style tag with no consequence
    */
    body {background: #E5E5E5; }
  </style>
</head>
<body>
  <div class="e52_1125">
    <div class="e52_1126">
      <div class="e52_1127"></div>
    </div>
    <div class="e52_1128">
      <div class="e52_1129"></div>
      <div class="e52_1130">
        <span class="e52_1131">El Poison Zombie es el zombi resultante de un huésped humano bajo el control de un Poison Headcrab . Las mutaciones que se producen producen un zombi hinchado y de movimiento lento que es capaz de portar cangrejos venenosos adicionales.</span>
      </div>
    </div>
    <div class="e52_1132">
      <div class="e52_1133"></div>
      <div class="e52_1134"></div>
    </div>
    <div class="e55_142">
      <div class="e55_143"></div>
      <!-- Agrega un enlace al otro HTML en el atributo "href" sin espacios adicionales -->
      <a href="../xen_1.php"><span class="e55_140"><span class="e55_144">Anterior</span></a>
      <div class="e55_145"></div>
    </div>
  </div>
</body>
</html>
