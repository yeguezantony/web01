<?php

// Iniciar la sesión
session_start(); 

// Incluir el archivo de autenticación
require 'C:/xampp/htdocs/proyecto_web/login/auth.php';

// Verificar que el usuario haya iniciado sesión 
checkLoggedIn();

?>
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <title>Proyecto Web</title>
  <meta name="description" content="Figma htmlGenerator">
  <meta name="Jose Camargo, Antony Yeguez, Greeberth Linares" content="htmlGenerator">
  <link href="https://fonts.googleapis.com/css?family=Nunito&display=swap" rel="stylesheet">
  <link rel="stylesheet" href="styles.css">
  <style>
    /* Figma Background for illustrative/preview purposes only.
       You can remove this style tag with no consequence */
    body {background: #E5E5E5; }
  </style>
</head>
<body>
  <div class="e52_1035">
    <div class="e52_1036">
      <div class="e52_1037"></div>
    </div>
    <div class="e52_1038">
      <div class="e52_1039"></div>
      <div class="e52_1040">
        <span class="e52_1041">El Headcrab rápido es una subespecie del Headcrab anteriormente desconocida antes de la invasión del Imperio Combine. Se desconoce si eran nativos de Xen, una mutación reciente, o como resultado de la manipulación genética de un headcrab por parte del imperio Combine para su uso como arma biológica.</span>
      </div>
    </div>
    <div class="e52_1042">
      <div class="e52_1043"></div>
      <div class="e52_1044"></div>
    </div>
    <!-- Agregamos el enlace alrededor del texto "Anterior" -->
    <a href="../xen_1.php" class="e55_126">
      <div class="e55_127"></div>
      <span class="e55_128">Anterior</span>
      <div class="e55_129"></div>
    </a>
  </div>
</body>
</html>
