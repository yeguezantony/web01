<?php

// Iniciar la sesión
session_start(); 

// Incluir el archivo de autenticación
require 'C:/xampp/htdocs/proyecto_web/login/auth.php';

// Verificar que el usuario haya iniciado sesión 
checkLoggedIn();

?>
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <title>Proyecto Web</title>
  <meta name="description" content="Figma htmlGenerator">
  <meta name="Jose Camargo, Antony Yeguez, Greeberth Linares" content="htmlGenerator">
  <link href="https://fonts.googleapis.com/css?family=Nunito&display=swap" rel="stylesheet">
  <link rel="stylesheet" href="styles.css">
  <style>
    /*
      Figma Background for illustrative/preview purposes only.
      You can remove this style tag with no consequence
    */
    body {background: #E5E5E5; }
  </style>
</head>
<body>
  <div class="e52_1103">
    <div class="e52_1104">
      <div class="e52_1105"></div>
    </div>
    <div class="e52_1106">
      <div class="e52_1107"></div>
      <div class="e52_1108">
        <span class="e52_1109">El Fast Zombie es el producto de un huésped humano que quedó bajo el control de un Fast Headcrab . Los zombis rápidos son extremadamente rápidos y ágiles, pudiendo trepar, correr y saltar más rápido y más lejos que un zombi normal. Carecen de piel u órganos y solo se componen de esqueleto y músculos. Su predecesor es el Zombie Assassin.</span>
      </div>
    </div>
    <div class="e52_1110">
      <div class="e52_1111"></div>
      <div class="e52_1112"></div>
    </div>
    <div class="e55_138">
      <div class="e55_139"></div>
      <!-- Agrega un enlace al otro HTML en el atributo "href" sin espacios adicionales -->
      <a href="../xen_1.php"><span class="e55_140">Anterior</span></a>
      <div class="e55_141"></div>
    </div>
  </div>
</body>
</html>
