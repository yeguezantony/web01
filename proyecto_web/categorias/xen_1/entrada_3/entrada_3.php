<?php

// Iniciar la sesión
session_start(); 

// Incluir el archivo de autenticación
require 'C:/xampp/htdocs/proyecto_web/login/auth.php';

// Verificar que el usuario haya iniciado sesión 
checkLoggedIn();

?>
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <title>Proyecto Web</title>
  <meta name="description" content="Figma htmlGenerator">
  <meta name="Jose Camargo, Antony Yeguez, Greeberth Linares" content="htmlGenerator">
  <link href="https://fonts.googleapis.com/css?family=Nunito&display=swap" rel="stylesheet">
  <link rel="stylesheet" href="styles.css">
  <style>
    /* Figma Background for illustrative/preview purposes only.
       You can remove this style tag with no consequence */
    body {background: #E5E5E5; }
  </style>
</head>
<body>
  <div class="e52_1057">
    <div class="e52_1058">
      <div class="e52_1059"></div>
    </div>
    <div class="e52_1060">
      <div class="e52_1061"></div>
      <div class="e52_1062">
        <span class="e52_1063">La segunda variación del Headcrab es el Poison Headcrab (también conocidos como Headcrabs Negros o Headcrabs Venenosos), es identificado por su piel verde oscura (a veces con un brillo mojado) y los pelos gruesos en sus piernas. Desde lejos se pueden distinguir por el pelo blanco cerca de las rodillas. Semejantes a los headcrabs rápidos, el headcrab venenoso tiene picos en su superficie inferior como los headcrabs normales. También hace un chirrido distintivamente siniestro. Los headcrabs venenosos se mueven lenta y cautelosamente al maniobrar, pero saltan con una velocidad y una altura increíbles acompañados por un chillido enojado cuando se aproxima a su víctima.
        </span>
      </div>
    </div>
    <div class="e52_1064">
      <div class="e52_1065"></div>
      <div class="e52_1066"></div>
    </div>
    <!-- Agregamos el enlace alrededor del texto "Anterior" -->
    <a href="../xen_1.php" class="e55_130">
      <div class="e55_131"></div>
      <span class="e55_132">Anterior</span>
      <div class="e55_133"></div>
    </a>
  </div>
</body>
</html>
