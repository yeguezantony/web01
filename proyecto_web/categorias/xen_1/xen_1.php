<?php

// Iniciar la sesión
session_start(); 

// Incluir el archivo de autenticación
require 'C:/xampp/htdocs/proyecto_web/login/auth.php';

// Verificar que el usuario haya iniciado sesión 
checkLoggedIn();

?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title>Proyecto Web</title>
    <meta name="description" content="Figma htmlGenerator">
    <meta name="Jose Camargo, Antony Yeguez, Greeberth Linares" content="htmlGenerator">
    <link href="https://fonts.googleapis.com/css?family=Nunito&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="styles.css">
    <style>
        /*
          Figma Background for illustrative/preview purposes only.
          You can remove this style tag with no consequence
        */
        body {background: #E5E5E5; }
    </style>
</head>
<body>
    <div class="e40_266">
        <div class="e40_267">
            <div class="e40_268"></div>
        </div>
        <div class="e40_269"></div>
        <span class="e40_271">Elige con un simple click sobre el personaje sobre el que quieres conocer mas información</span>
        <div class="e40_272">
            <a href="entrada_1/entrada_1.php">
                <div class="e40_273"></div>
                <div class="e40_274"></div>
                <div class="e40_277"></div>
                <div class="e40_275"></div>
                <span class="e40_276">Ver contenido</span>
                <span class="e40_278">Headcrab</span>
            </a>
        </div>
        <div class="e40_279">
           <a href="entrada_4/entrada_4.php">
                <div class="e44_333"></div>
                <div class="e44_334"></div>
                <div class="e44_335"></div>
                <div class="e40_283"></div>
                <span class="e40_284">Ver contenido</span>
                <span class="e40_285">Zombi</span>
            </a>
        </div>
        <div class="e40_286">
            <a href="entrada_5/entrada_5.php">
                <div class="e44_336"></div>
                <div class="e44_337"></div>
                <div class="e44_338"></div>
                <div class="e40_290"></div>
                <span class="e40_291">Ver contenido</span>
                <span class="e40_292">Zombi Rápido</span>
            </a>
        </div>
        <div class="e40_293">
            <a href="entrada_6/entrada_6.php">
                <div class="e44_339"></div>
                <div class="e44_340"></div>
                <div class="e44_341"></div>
                <div class="e40_297"></div>
                <span class="e40_298">Ver contenido</span>
                <span class="e40_299">Zombi Venenoso</span>
            </a>
        </div>
        <div class="e40_300">
            <a href="entrada_2/entrada_2.php">
                <div class="e44_327"></div>
                <div class="e44_328"></div>
                <div class="e44_329"></div>
                <div class="e40_304"></div>
                <span class="e40_305">Ver contenido</span>
                <span class="e40_306">Headcrab Rápido</span>
            </a>
        </div>
        <div class="e40_307">
            <a href="entrada_3/entrada_3.php">
                <div class="e44_330"></div>
                <div class="e44_331"></div>
                <div class="e44_332"></div>
                <div class="e40_311"></div>
                <span class="e40_312">Ver contenido</span>
                <span class="e40_313">Headcrab Venenoso</span>
            </a>
        </div>
        <div class="e40_314">
            <a href="../xen_2/xen_2.php">
	    <div class="e40_315"></div>
            <span class="e40_316">Siguiente</span>
            <div class="e40_317"></div>
        </div>
        <div class="e40_318">
            <a href="../categorias.php">
                <div class="e40_319"></div>
                <span class="e40_320">Categorias</span>
            </a>
            <div class="e40_321"></div>
        </div>
        <div class="e40_322">
            <a href="../../home/pag_principal.php">
                <div class="e40_323"></div>
                <span class="e40_324">Inicio</span>
            </a>
            <div class="e40_325"></div>
        </div>
        <span class="e40_326">Xen es un plano de existencia interdimensional que fue descubierto por los científicos de Black Mesa. Los científicos viajaron regularmente a Xen para estudiar su flora y fauna, a pesar del alto riesgo. Xen es el hogar de la raza Nihilanth y es el origen de la mayoría de las especies alienígenas que aparecen en la Saga Half-Life.</span>
    </div>
</body>
</html>
