<?php

// Iniciar la sesión
session_start(); 

// Incluir el archivo de autenticación
require 'C:/xampp/htdocs/proyecto_web/login/auth.php';

// Verificar que el usuario haya iniciado sesión 
checkLoggedIn();

?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title>Proyecto Web</title>
    <meta name="description" content="Figma htmlGenerator">
    <meta name="Jose Camargo, Antony Yeguez, Greeberth Linares" content="htmlGenerator">
    <link href="https://fonts.googleapis.com/css?family=Nunito&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="styles.css">
    <style>
        /*
          Figma Background for illustrative/preview purposes only.
          You can remove this style tag with no consequence
        */
        body {background: #E5E5E5; }
    </style>
</head>
<body>
    <div class="e52_858">
        <div class="e52_859">
            <div class="e52_860"></div>
            <div class="e55_66">
                <div class="e55_67"></div>
                <!-- Cambia el atributo href para redirigir a la página deseada -->
                <a href="../combine_2.php"><span class="e55_68">Anterior</span></a>
                <div class="e55_69"></div>
            </div>
        </div>
        <div class="e52_861">
            <div class="e52_862"></div>
            <div class="e52_863">
                <span class="e52_864">El City Scanner , también conocido como Scanner Type, y a menudo denominado simplemente Scanner , es una cámara de seguridad voladora ligeramente blindada utilizada por Combine para monitorear a los residentes de City 17 (y posiblemente de otras ciudades). Su contraparte Synth es el Shield Scanner .
                </span>
            </div>
        </div>
        <div class="e52_865">
            <div class="e52_866"></div>
            <div class="e52_867"></div>
        </div>
    </div>
</body>
</html>
