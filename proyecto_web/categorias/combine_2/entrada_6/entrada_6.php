<?php

// Iniciar la sesión
session_start(); 

// Incluir el archivo de autenticación
require 'C:/xampp/htdocs/proyecto_web/login/auth.php';

// Verificar que el usuario haya iniciado sesión 
checkLoggedIn();

?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title>Proyecto Web</title>
    <meta name="description" content="Figma htmlGenerator">
    <meta name="Jose Camargo, Antony Yeguez, Greeberth Linares" content="htmlGenerator">
    <link href="https://fonts.googleapis.com/css?family=Nunito&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="styles.css">
    <style>
        /* Figma Background for illustrative/preview purposes only.
           You can remove this style tag with no consequence
        */
        body { background: #E5E5E5; }
    </style>
</head>
<body>
    <div class="e52_969">
        <div class="e52_970">
            <div class="e52_971"></div>
        </div>
        <div class="e52_972">
            <div class="e52_973"></div>
            <div class="e52_974">
                <span class="e52_975">El Shield Scanner es comúnmente utilizado por las fuerzas Combine para lanzar Hopper Mines en áreas de combate y está equipado con un gran brazo de pinza que se mantiene oculto detrás de una placa blindada. Estas minas generalmente se arrojan en áreas en las que las fuerzas Combine intentan limitar el movimiento del enemigo, como calles y callejones, aunque ocasionalmente el escáner intentará arrojar la mina encima del jugador.
Cuando no se manipulan minas, los Shield Scanners se utilizan con fines de exploración y reconocimiento. A veces también acompañan a los Striders y actúan como observadores, buscando en los edificios para informar las posiciones de los objetivos que pueden estar escondidos en su interior y que de otro modo el Strider no podría ver.
                </span>
            </div>
        </div>
        <div class="e52_976">
            <div class="e52_977"></div>
            <div class="e52_978"></div>
        </div>
        <div class="e55_86">
            <div class="e55_87"></div>
            <a href="../combine_2.php"><span class="e55_88">Anterior</span></a>
            <div class="e55_89"></div>
        </div>
    </div>
</body>
</html>
