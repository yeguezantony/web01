<?php

// Iniciar la sesión
session_start(); 

// Incluir el archivo de autenticación
require 'C:/xampp/htdocs/proyecto_web/login/auth.php';

// Verificar que el usuario haya iniciado sesión 
checkLoggedIn();

?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title>Proyecto Web</title>
    <meta name="description" content="Figma htmlGenerator">
    <meta name="Jose Camargo, Antony Yeguez, Greeberth Linares" content="htmlGenerator">
    <link href="https://fonts.googleapis.com/css?family=Nunito&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="styles.css">
    <style>
        /* Figma Background for illustrative/preview purposes only.
           You can remove this style tag with no consequence
        */
        body { background: #E5E5E5; }
    </style>
</head>
<body>
    <div class="e52_947">
        <div class="e52_948">
            <div class="e52_949"></div>
        </div>
        <div class="e52_950">
            <div class="e52_951"></div>
            <div class="e52_952">
                <span class="e52_953">El Cazador, originalmente apodado Mini Strider en los archivos del juego del Episodio Uno, es un sintetizador rápido y ágil utilizado por Combine como explorador y escolta. El Cazador se ve por primera vez en el mensaje de Mossman en Half-Life 2: Episodio uno, pero no se encuentra directamente hasta el Episodio dos, donde es un enemigo prominente y un minijefe.
                </span>
            </div>
        </div>
        <div class="e52_954">
            <div class="e52_955"></div>
            <div class="e52_956"></div>
        </div>
        <div class="e55_82">
            <div class="e55_83"></div>
            <a href="../combine_2.php"><span class="e55_84">Anterior</span></a>
            <div class="e55_85"></div>
        </div>
    </div>
</body>
</html>
