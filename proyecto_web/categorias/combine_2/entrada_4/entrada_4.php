<?php

// Iniciar la sesión
session_start(); 

// Incluir el archivo de autenticación
require 'C:/xampp/htdocs/proyecto_web/login/auth.php';

// Verificar que el usuario haya iniciado sesión 
checkLoggedIn();

?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title>Proyecto Web</title>
    <meta name="description" content="Figma htmlGenerator">
    <meta name="Jose Camargo, Antony Yeguez, Greeberth Linares" content="htmlGenerator">
    <link href="https://fonts.googleapis.com/css?family=Nunito&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="styles.css">
    <style>
        /* Figma Background for illustrative/preview purposes only.
           You can remove this style tag with no consequence
        */
        body { background: #E5E5E5; }
    </style>
</head>
<body>
    <div class="e52_925">
        <div class="e52_926">
            <div class="e52_927"></div>
        </div>
        <div class="e52_928">
            <div class="e52_929"></div>
            <div class="e52_930">
                <span class="e52_931">La parte frontal y los dos pares de "piernas" más atrás en la nave contienen motores que brindan elevación y movimiento. Las dos piernas frontales también tienen alas retráctiles utilizadas para maniobrar. Las cuatro patas centrales terminan en ventosas de gran tamaño que se utilizan para llevar a las distintas formas de carga. Estas Naves por sí mismas están desarmadas y por eso no atacan al jugador. Sin embargo, los contenedores comúnmente cargados por estas suelen tener una torreta de pulso montada en la parte frontal, diseñada para suprimir cualquier oposición frente a la nave, lo que permite a los ocupantes desplegarse de forma segura.
                </span>
            </div>
        </div>
        <div class="e52_932">
            <div class="e52_933"></div>
            <div class="e52_934"></div>
        </div>
        <div class="e55_78">
            <div class="e55_79"></div>
            <a href="../combine_2.php"><span class="e55_80">Anterior</span></a>
            <div class="e55_81"></div>
        </div>
    </div>
</body>
</html>
