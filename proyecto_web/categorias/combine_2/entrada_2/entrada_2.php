<?php

// Iniciar la sesión
session_start(); 

// Incluir el archivo de autenticación
require 'C:/xampp/htdocs/proyecto_web/login/auth.php';

// Verificar que el usuario haya iniciado sesión 
checkLoggedIn();

?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title>Proyecto Web</title>
    <meta name="description" content="Figma htmlGenerator">
    <meta name="Jose Camargo, Antony Yeguez, Greeberth Linares" content="htmlGenerator">
    <link href="https://fonts.googleapis.com/css?family=Nunito&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="styles.css">
    
    <style>
        /* Figma Background for illustrative/preview purposes only. You can remove this style tag with no consequence */
        body { background: #E5E5E5; }
    </style>
</head>

<body>
    <div class="e52_881">
        <div class="e52_882">
            <div class="e52_883"></div>
        </div>
        <div class="e52_884">
            <div class="e52_885"></div>
            <div class="e52_886">
                <span class="e52_887">
                    Los zancudos son capaces de perseguir enemigos tanto en calles confinadas como en plazas abiertas, incluso agachándose para disparar a los enemigos que buscan refugio bajo salientes o en edificios bombardeados.
                    El cuerpo del Strider está cubierto por un caparazón o exoesqueleto marrón liso. En la "cabeza" del Strider hay un cañón de pulsos de disparo rápido, con un cañón warp más pesado suspendido debajo. Las tres largas patas del Zancudo tienen puntas afiladas y una roseta de espinas más finas, parecidas a pelos.
                </span>
            </div>
        </div>
        <div class="e52_888">
            <div class="e52_889"></div>
            <div class="e52_890"></div>
        </div>
        <div class="e55_70">
            <div class="e55_71"></div>
            <a href="../combine_2.php"><span class="e55_72">Anterior</span></a>
            <div class="e55_73"></div>
        </div>
    </div>
</body>
</html>
