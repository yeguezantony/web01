<?php

// Iniciar la sesión
session_start(); 

// Incluir el archivo de autenticación
require 'C:/xampp/htdocs/proyecto_web/login/auth.php';

// Verificar que el usuario haya iniciado sesión 
checkLoggedIn();

?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title>Proyecto Web</title>
    <meta name="description" content="Figma htmlGenerator">
    <meta name="Jose Camargo, Antony Yeguez, Greeberth Linares" content="htmlGenerator">
    <link href="https://fonts.googleapis.com/css?family=Nunito&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="styles.css">
    <style>
        /* Figma Background for illustrative/preview purposes only.
           You can remove this style tag with no consequence
        */
        body { background: #E5E5E5; }
    </style>
</head>
<body>
    <div class="e52_903">
        <div class="e52_904">
            <div class="e52_905"></div>
        </div>
        <div class="e52_906">
            <div class="e52_907"></div>
            <div class="e52_908">
                <span class="e52_909">El Gunship es una unidad de asalto aéreo Synth cuya apariencia general se asemeja a la de un animal acuático. Tiene una cabeza redonda y rechoncha con un cuerpo que se extiende más hacia su parte trasera y termina con un enorme rotor redondo que forma su "cola". Dos aletas están ubicadas cerca de su cabeza junto con dos extremidades pequeñas y delgadas cerca de su rotor. El caparazón del Gunship está dividido en dos secciones: la parte superior de la mitad del Gunship tiene un patrón de camuflaje que consiste en un color verde-marrón mezclado con manchas de color blanco que conducen a un color más naranja en la cola. La parte inferior es blanca con detalles en verde azulado debido a la tecnología Combine incorporada. Curiosamente, el Gunship muestra una forma de contrasombreado, que se ve a menudo en la vida marina, en la que la parte superior del cuerpo tiene un color más oscuro que las áreas inferiores.
                </span>
            </div>
        </div>
        <div class="e52_910">
            <div class="e52_911"></div>
            <div class="e52_912"></div>
        </div>
        <div class="e55_74">
            <div class="e55_75"></div>
            <a href="../combine_2.php"><span class="e55_76">Anterior</span></a>
            <div class="e55_77"></div>
        </div>
    </div>
</body>
</html>
