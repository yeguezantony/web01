<?php

// Iniciar la sesión
session_start(); 

// Incluir el archivo de autenticación
require 'C:/xampp/htdocs/proyecto_web/login/auth.php';

// Verificar que el usuario haya iniciado sesión 
checkLoggedIn();

?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title>Proyecto Web</title>
    <meta name="description" content="Figma htmlGenerator">
    <meta name="Jose Camargo, Antony Yeguez, Greeberth Linares" content="htmlGenerator">
    <link href="https://fonts.googleapis.com/css?family=Nunito&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="styles.css">
    <style>
        /*
          Figma Background for illustrative/preview purposes only.
          You can remove this style tag with no consequence
        */
        body {background: #E5E5E5; }
    </style>
</head>
<body>
    <div class="e40_103">
        <div class="e40_104">
            <div class="e40_105"></div>
        </div>
        <div class="e40_109">
            <a href="entrada_1/entrada_1.php">
                <div class="e40_110"></div>
                <div class="e40_111"></div>
                <div class="e40_112"></div>
                <span class="e40_113">Ver contenido</span>
            </a>
            <div class="e40_114"></div>
            <span class="e40_115">City Scanner</span>
        </div>
        <div class="e40_116">
            <a href="entrada_4/entrada_4.php">
                <div class="e40_117"></div>
                <div class="e40_118"></div>
                <div class="e40_119"></div>
                <div class="e40_120"></div>
                <span class="e40_121">Ver contenido</span>
            </a>
            <span class="e40_122">Aerotransportador</span>
        </div>
        <div class="e40_123">
            <a href="entrada_5/entrada_5.php">
                <div class="e40_124"></div>
                <div class="e40_125"></div>
                <div class="e40_126"></div>
                <div class="e40_127"></div>
                <span class="e40_128">Ver contenido</span>
            </a>
            <span class="e40_129">Hunter</span>
        </div>
        <div class="e40_130">
            <a href="entrada_6/entrada_6.php">
                <div class="e40_131"></div>
                <div class="e40_132"></div>
                <div class="e40_133"></div>
                <div class="e40_134"></div>
                <span class="e40_135">Ver contenido</span>
            </a>
            <span class="e40_136">Shield Scanner</span>
        </div>
        <div class="e40_137">
            <a href="entrada_2/entrada_2.php">
                <div class="e40_138"></div>
                <div class="e40_139"></div>
                <div class="e40_140"></div>
                <div class="e40_141"></div>
                <span class="e40_142">Ver contenido</span>
            </a>
            <span class="e40_143">Strider</span>
        </div>
        <div class="e40_144">
            <a href="entrada_3/entrada_3.php">
                <div class="e40_145"></div>
                <div class="e40_146"></div>
                <div class="e40_147"></div>
                <div class="e40_148"></div>
                <span class="e40_149">Ver contenido</span>
            </a>
            <span class="e40_150">Combine Gunship</span>
        </div>
        <div class="e40_240">
            <a href="../combine_3/combine_3.php">
                <div class="e40_241"></div>
                <span class="e40_242">Siguiente</span>
            </a>
            <div class="e40_243"></div>
        </div>
        <div class="e40_244">
            <a href="../combine_1/combine_1.php">
                <div class="e40_245"></div>
                <div class="e40_247"></div>
                <span class="e47_403">Anterior</span>
            </a>
        </div>
        <div class="e40_248">
            <a href="../../home/pag_principal.php">
                <div class="e40_249"></div>
                <span class="e40_250">Inicio</span>
            </a>
            <div class="e40_251"></div>
        </div>
        <div class="e40_107">
            <span class="e40_108">Elige con un simple click sobre el personaje sobre el que quieres conocer más información</span>
        </div>
        <div class="e40_264"></div>
    </div>
</body>
</html>
