<?php

// Iniciar la sesión
session_start(); 

// Incluir el archivo de autenticación
require 'C:/xampp/htdocs/proyecto_web/login/auth.php';

// Verificar que el usuario haya iniciado sesión 
checkLoggedIn();

?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title>Proyecto Web</title>
    <meta name="description" content="Figma htmlGenerator">
    <meta name="Jose Camargo, Antony Yeguez, Greeberth Linares" content="htmlGenerator">
    <link href="https://fonts.googleapis.com/css?family=Nunito&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="styles.css">
    <style>
        /* Figma Background for illustrative/preview purposes only.
           You can remove this style tag with no consequence
        */
        body {background: #E5E5E5; }
    </style>
</head>
<body>
    <div class="e52_991">
        <div class="e52_992">
            <div class="e52_993"></div>
        </div>
        <div class="e52_994">
            <div class="e52_995"></div>
            <div class="e52_996">
                <span class="e52_997">Los Consejeros Combine son criaturas grandes, pálidas y acéfalas. Poseen gran inteligencia y carecen de rasgos faciales discernibles. Tienen un dispositivo similar a una máscara de gas unido a su extremo delantero, así como un ojo cibernético en el lado izquierdo de la placa frontal. Cada Consejero lleva un traje verde oliva ceñido que los cubre enteramente, excepto los extremos de sus cuerpos, y llevan un collar adornado con glifos de oro alrededor de sus cuellos. Tienen además un par de delgados brazos robóticos negros implantados a sus espaldas, capaces de agarrar y levantar a un humano adulto, así como mover el peso del Consejero. Poseen un apéndice similar a una lengua en apariencia, que se utiliza de manera análoga a una trompa y se sumerge en un punto débil como el cuello humano, tal como se muestra a partir de una escena en Half-Life 2: Episode Two.
                </span>
            </div>
        </div>
        <div class="e52_998">
            <div class="e52_999"></div>
            <div class="e52_1000"></div>
        </div>
        <div class="e55_90">
            <div class="e55_91"></div>
            <a href="../combine_3.php"><span class="e55_92">Anterior</span></a>
            <div class="e55_93"></div>
        </div>
    </div>
</body>
</html>
