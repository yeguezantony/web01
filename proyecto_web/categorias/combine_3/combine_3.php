<?php

// Iniciar la sesión
session_start(); 

// Incluir el archivo de autenticación
require 'C:/xampp/htdocs/proyecto_web/login/auth.php';

// Verificar que el usuario haya iniciado sesión 
checkLoggedIn();

?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title>Proyecto Web</title>
    <meta name="description" content="Figma htmlGenerator">
    <meta name="Jose Camargo, Antony Yeguez, Greeberth Linares" content="htmlGenerator">
    <link href="https://fonts.googleapis.com/css?family=Nunito&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="styles.css">
    <style>
        /*
          Figma Background for illustrative/preview purposes only.
          You can remove this style tag with no consequence
        */
        body {background: #E5E5E5; }
    </style>
</head>
<body>
    <div class="e40_168">
        <div class="e40_169">
            <div class="e40_170"></div>
        </div>
        <div class="e40_174">
            <a href="entrada_1/entrada_1.php">
                <div class="e40_175"></div>
                <div class="e40_176"></div>
                <div class="e40_177"></div>
                <span class="e40_178">Ver contenido</span>
            </a>
            <div class="e40_179"></div>
            <span class="e40_180">Consejero Combine</span>
        </div>
        <div class="e40_252">
            <a href="../combine_2/combine_2.php">
                <div class="e40_253"></div>
                <span class="e40_254">Anterior</span>
            </a>
            <div class="e47_408"></div>
        </div>
        <div class="e40_256">
            <a href="../categorias.php">
                <div class="e40_257"></div>
                <span class="e47_404">Categorias</span>
            </a>
            <div class="e40_259"></div>
        </div>
        <div class="e40_260">
            <a href="../../home/pag_principal.php">
                <div class="e40_261"></div>
                <span class="e40_262">Inicio</span>
            </a>
            <div class="e40_263"></div>
        </div>
        <div class="e40_265"></div>
        <div class="e40_172">
            <span class="e40_173">Elige con un simple click sobre el personaje sobre el que quieres conocer más información</span>
        </div>
    </div>
</body>
</html>
