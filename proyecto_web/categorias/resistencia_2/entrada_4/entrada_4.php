<?php

// Iniciar la sesión
session_start(); 

// Incluir el archivo de autenticación
require 'C:/xampp/htdocs/proyecto_web/login/auth.php';

// Verificar que el usuario haya iniciado sesión 
checkLoggedIn();

?>
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <title>Proyecto Web</title>
  <meta name="description" content="Figma htmlGenerator">
  <meta name="Jose Camargo, Antony Yeguez, Greeberth Linares" content="htmlGenerator">
  <link href="https://fonts.googleapis.com/css?family=Nunito&display=swap" rel="stylesheet">
  <link rel="stylesheet" href="styles.css">
  <style>
    /* Figma Background for illustrative/preview purposes only.
       You can remove this style tag with no consequence */
    body {background: #E5E5E5; }
  </style>
</head>
<body>
  <div class="e49_699">
    <div class="e49_700">
      <div class="e49_701"></div>
      <div class="e49_702"></div>
    </div>
    <div class="e49_703">
      <div class="e49_704"></div>
      <div class="e49_705">
        <span class="e49_706">Los Vortigaunt, anteriormente conocidos como Esclavos Alienígenas y conocidos cariñosamente como "Vorts" por sus aliados, son especies exóticas sapientes encontradas en la Saga Half-Life. 
Mucho antes del Incidente de Black Mesa, el mundo natal de los Vortigaunts fue invadido por los Combine, obligando a los sobrevivientes de la invasión a huir a Xen. Cuando ocurrió el Incidente de Black Mesa, su maestro, un ser conocido como el Nihilanth, vio la grieta que se abría como una oportunidad para escapar, y dirigió a los Vortigaunts para que invadieran la Tierra. Tras la muerte de Nihilanth, los Vortigaunts optaron por aliarse con la Resistencia humana en su intento de derrocar al Imperio Combine en la Tierra.
        </span>
      </div>
    </div>
    <div class="e49_707">
      <div class="e49_708"></div>
      <div class="e49_709"></div>
    </div>
    <!-- Agregamos el enlace alrededor del texto "Anterior" -->
    <a href="../resistencia_2.php" class="e55_34">
      <div class="e55_35"></div>
      <span class="e55_36">Anterior</span>
      <div class="e55_37"></div>
    </a>
  </div>
</body>
</html>
