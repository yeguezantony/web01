<?php

// Iniciar la sesión
session_start(); 

// Incluir el archivo de autenticación
require 'C:/xampp/htdocs/proyecto_web/login/auth.php';

// Verificar que el usuario haya iniciado sesión 
checkLoggedIn();

?>
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <title>Proyecto Web</title>
  <meta name="description" content="Figma htmlGenerator">
  <meta name="Jose Camargo, Antony Yeguez, Greeberth Linares" content="htmlGenerator">
  <link href="https://fonts.googleapis.com/css?family=Nunito&display=swap" rel="stylesheet">
  <link rel="stylesheet" href="styles.css">
  <style>
    /* Figma Background for illustrative/preview purposes only.
       You can remove this style tag with no consequence */
    body {background: #E5E5E5; }
  </style>
</head>
<body>
  <div class="e49_630">
    <div class="e49_631">
      <div class="e49_632"></div>
      <div class="e49_633"></div>
    </div>
    <div class="e49_634">
      <div class="e49_635"></div>
      <div class="e49_636">
        <span class="e49_637">En el episodio dos , el Dr. Arne Magnusson (con la voz de John Aylward ) dirige la base del Bosque Blanco y es descrito como un sobreviviente de Black Mesa. No se lleva bien con el Dr. Kleiner debido a sus personalidades encontradas, como lo indican sus propios nombres: " Magnus " significa "grande" en latín, mientras que "klein" significa "pequeño" en alemán y holandés. La peculiar personalidad de Magnusson parece haberle ganado mucho respeto por parte de los Vortigaunts , como su asistente Uriah, quien hace referencias asombradas a él.
Magnusson también le hace un comentario a Freeman diciéndole que si defiende con éxito White Forest, entonces perdonará a Freeman por un incidente anterior en Black Mesa, que involucra su 'Cazuela de microondas', una referencia a una escena en el primer Half- Life .
        </span>
      </div>
    </div>
    <div class="e49_638">
      <div class="e49_639"></div>
      <div class="e49_640"></div>
    </div>
    <!-- Agregamos el enlace alrededor del texto "Anterior" -->
    <a href="../resistencia_2.php" class="e55_30">
      <div class="e55_31"></div>
      <span class="e55_32">Anterior</span>
      <div class="e55_33"></div>
    </a>
  </div>
</body>
</html>
