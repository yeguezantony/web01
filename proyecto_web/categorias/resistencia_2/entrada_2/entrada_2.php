<?php

// Iniciar la sesión
session_start(); 

// Incluir el archivo de autenticación
require 'C:/xampp/htdocs/proyecto_web/login/auth.php';

// Verificar que el usuario haya iniciado sesión 
checkLoggedIn();

?>
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <title>Proyecto Web</title>
  <meta name="description" content="Figma htmlGenerator">
  <meta name="Jose Camargo, Antony Yeguez, Greeberth Linares" content="htmlGenerator">
  <link href="https://fonts.googleapis.com/css?family=Nunito&display=swap" rel="stylesheet">
  <link rel="stylesheet" href="styles.css">
  <style>
    /* Figma Background for illustrative/preview purposes only.
       You can remove this style tag with no consequence */
    body {background: #E5E5E5; }
  </style>
</head>
<body>
  <div class="e49_653">
    <div class="e49_654">
      <div class="e49_655"></div>
      <div class="e49_656"></div>
    </div>
    <div class="e49_657">
      <div class="e49_658"></div>
      <div class="e49_659">
        <span class="e49_660">Dog es un enorme robot parecido a un gorila que pertenece a Alyx Vance, construido por su padre Eli para brindarle compañía y protección. Posteriormente, Alyx actualizó el robot a su forma actual. A pesar de su nombre, Perro tiene apariencia antropomorfa. Dog brinda apoyo a Freeman durante el entrenamiento con Gravity Gun y hace apariciones varias veces después.
        </span>
      </div>
    </div>
    <div class="e49_661">
      <div class="e49_662"></div>
      <div class="e49_663"></div>
    </div>
    <!-- Agregamos el enlace alrededor del texto "Anterior" -->
    <a href="../resistencia_2.php" class="e55_26">
      <div class="e55_27"></div>
      <span class="e55_28">Anterior</span>
      <div class="e55_29"></div>
    </a>
  </div>
</body>
</html>
