<?php

// Iniciar la sesión
session_start(); 

// Incluir el archivo de autenticación
require 'C:/xampp/htdocs/proyecto_web/login/auth.php';

// Verificar que el usuario haya iniciado sesión 
checkLoggedIn();

?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title>Proyecto Web</title>
    <meta name="description" content="Figma htmlGenerator">
    <meta name="Jose Camargo, Antony Yeguez, Greeberth Linares" content="htmlGenerator">
    <link href="https://fonts.googleapis.com/css?family=Nunito&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="styles.css">
    <style>
        /*
          Figma Background for illustrative/preview purposes only.
          You can remove this style tag with no consequence
        */
        body {background: #E5E5E5; }
    </style>
</head>
<body>
    <div class="e25_6">
        <div class="e25_7">
            <div class="e25_8"></div>
            <div class="e25_9"></div>
            <div class="e25_10">
                <span class="e25_11">Elige con un simple click sobre el personaje sobre el que quieres conocer más información</span>
            </div>
        </div>
        <div class="e25_12">
            <a href="entrada_1/entrada_1.php">
                <div class="e25_13"></div>
                <div class="e25_14"></div>
                <div class="e25_15"></div>
                <span class="e25_16">Ver contenido</span>
            </a>
            <div class="e25_17"></div>
            <span class="e25_18">Arne Magnusson</span>
        </div>
        <div class="e47_442">
            <a href="entrada_4/entrada_4.php">
                <div class="e47_443"></div>
                <div class="e47_444"></div>
                <div class="e47_445"></div>
                <span class="e47_446">Ver contenido</span>
            </a>
            <div class="e47_447"></div>
            <span class="e47_448">Vortigaunt</span>
        </div>
        <div class="e25_40">
            <a href="entrada_2/entrada_2.php">
                <div class="e25_41"></div>
                <div class="e25_42"></div>
                <div class="e25_43"></div>
                <span class="e25_44">Ver contenido</span>
            </a>
            <div class="e25_45"></div>
            <span class="e25_46">Dog</span>
        </div>
        <div class="e25_47">
            <a href="entrada_3/entrada_3.php">
                <div class="e25_48"></div>
                <div class="e25_49"></div>
                <div class="e25_50"></div>
                <span class="e25_51">Ver contenido</span>
            </a>
            <div class="e25_52"></div>
            <span class="e25_53">Odessa Cubbage</span>
        </div>
        <div class="e25_54">
            <a href="../resistencia_1/resistencia_1.php">
                <div class="e25_55"></div>
                <span class="e25_56">Anterior</span>
            </a>
            <div class="e25_66"></div>
        </div>
        <div class="e25_58">
            <a href="../categorias.php">
                <div class="e25_59"></div>
                <span class="e25_60">Categorías</span>
            </a>
            <div class="e25_61"></div>
        </div>
        <div class="e25_62">
            <a href="../../home/pag_principal.php">
                <div class="e25_63"></div>
                <span class="e25_64">Inicio</span>
            </a>
            <div class="e25_65"></div>
        </div>
    </div>
</body>
</html>
