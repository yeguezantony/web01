<?php

// Iniciar la sesión
session_start(); 

// Incluir el archivo de autenticación
require 'C:/xampp/htdocs/proyecto_web/login/auth.php';

// Verificar que el usuario haya iniciado sesión 
checkLoggedIn();

?>
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <title>Proyecto Web</title>
  <meta name="description" content="Figma htmlGenerator">
  <meta name="Jose Camargo, Antony Yeguez, Greeberth Linares" content="htmlGenerator">
  <link href="https://fonts.googleapis.com/css?family=Nunito&display=swap" rel="stylesheet">
  <link rel="stylesheet" href="styles.css">
  <style>
    /* Figma Background for illustrative/preview purposes only.
       You can remove this style tag with no consequence */
    body {background: #E5E5E5; }
  </style>
</head>
<body>
  <div class="e49_676">
    <div class="e49_677">
      <div class="e49_678"></div>
      <div class="e49_679"></div>
    </div>
    <div class="e49_680">
      <div class="e49_681"></div>
      <div class="e49_682">
        <span class="e49_683">Odessa Cubbage lidera una pequeña base y ciudad de la Resistencia, apodada " Nueva Pequeña Odessa ", en una región costera en las afueras de Ciudad 17 . Antes de llegar a New Little Odessa, el jugador puede ver a Cubbage hablando con G-Man mirando a través de un telescopio binocular. Cuando Gordon Freeman llega a New Little Odessa de camino a Nova Prospekt , Cubbage está informando a los miembros sobre el uso del lanzacohetes contra las cañoneras Combine . Cubbage le confía el lanzacohetes a Gordon y nunca aparece para luchar él mismo, sino que se queda atrás para intentar contactar con otro asentamiento de la Resistencia.
        </span>
      </div>
    </div>
    <div class="e49_684">
      <div class="e49_685"></div>
      <div class="e49_686"></div>
    </div>
    <!-- Agregamos el enlace alrededor del texto "Anterior" -->
    <a href="../resistencia_2.php" class="e55_38">
      <div class="e55_39"></div>
      <span class="e55_40">Anterior</span>
      <div class="e55_41"></div>
    </a>
  </div>
</body>
</html>
