<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <title>Proyecto Web</title>
    <meta name="description" content="Figma htmlGenerator">
    <meta name="Jose Camargo, Antony Yeguez, Greeberth Linares" content="htmlGenerator">
    <link href="https://fonts.googleapis.com/css?family=Nunito&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="styles.css">
    <style>
      /*
        Figma Background for illustrative/preview purposes only.
        You can remove this style tag with no consequence
      */
      body {background: #E5E5E5; }
    </style>
  </head>
  
  <body>
    <div class="e8_3">
      <div class="e1_17">
        <div class="e3_2"></div>
        <div class="e1_4"></div>
        <div class="e1_10"></div>
        <div class="e1_12"></div>
        <div class="e3_3"></div>
        <div class="e9_8"></div>
        <div class="e9_9"></div>
        <div class="e9_21">
          <a href="categorias/categorias.php">
            <span class="e1_5">Ver contenido</span>
          </a>
          <span class="e1_8">Citizens of City 17</span>
          <span class="e3_16">El hombre adecuado en el lugar equivocado puede marcar la diferencia en el mundo</span>
          <span class="e1_7">Esta página contiene información sobre todos los personajes de Half-Life 2 y sus expansiones</span>
          <a href="login/Login.php">
            <span class="e1_13">Iniciar sesión</span>
          </a>
          <a href="login/registro.php">
            <span class="e1_16">Registrarse</span>
          </a>
          <a href="sobre_nosotros/sobre_nosotros.php">
            <span class="e9_5">Sobre nosotros</span>
          </a>
          <a href="contacto/contacto.php">
            <span class="e9_7">Contacto</span>
          </a>
          </div>
      </div>
    </div>
  </body>
</html>
