<?php

// Iniciar la sesión
session_start(); 

// Incluir el archivo de autenticación
require '../login/auth.php';

// Verificar que el usuario haya iniciado sesión 
checkLoggedIn();

?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title>Proyecto Web</title>
    <meta name="description" content="Figma htmlGenerator">
    <meta name="Jose Camargo, Antony Yeguez, Greeberth Linares" content="htmlGenerator">
    <link href="https://fonts.googleapis.com/css?family=Nunito&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="styles.css">
    <style>
        /*
          Figma Background for illustrative/preview purposes only.
          You can remove this style tag with no consequence
        */
        body {background: #E5E5E5; }
    </style>
</head>
<body>
    <div class="e55_166">
        <div class="e55_167">
            <div class="e55_168"></div>
            <div class="e55_169"></div>
            <div class="e55_170">
                <span class="e55_171">Elige Entre los Diferentes Contactos para Comunicarte con Nosotros</span>
            </div>
        </div>
        <div class="e55_172">
            <div class="e55_173"></div>
            <span class="e55_174">Gmail: ejemplogmail.com</span>
            <div class="e55_175"></div>
        </div>
        <div class="e55_176">
            <div class="e55_177"></div>
            <span class="e55_178">WhatsApp: 1234567890</span>
            <div class="e55_179"></div>
        </div>
        <div class="e55_180">
            <div class="e55_181"></div>
            <span class="e55_182">Instagram: ejem_plo</span>
            <div class="e55_183"></div>
        </div>
        <div class="e55_188">
            <div class="e55_189"></div>
            <a href="../home/pag_principal.php"><span class="e55_190">Inicio</span></a>
            <div class="e55_191"></div>
        </div>
    </div>
</body>
</html>
